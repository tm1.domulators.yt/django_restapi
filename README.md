# Django_RESTAPI

### Local Application setup
To setup the application locally simply clone the app, 
open a cmd and go to the application director.
<br>
When running for the first time, enter the following command:
<br><br>
```docker-compose up --build``` 
<br><br>
Which will build your app image and store it in your shared drive with docker. 
Once you have your app container running locally, simply go to the following link
<br><br>
[http://localhost:8000](http://localhost:8000)
<br><br>
Now you can see the swagger documentation for the app. Try hitting any end point
with the set of data you want and you can see those data being populated. You can also use 
django admin to view and update the data.
<br><br>
Before running migrations lets create an admin user in our postgres:
<br>
```hit win key and enter "open psql"``` <br>
```Server [localhost]:
Database [postgres]:
Port [5432]:
Username [postgres]:
Password for user postgres:
psql (12.2)
WARNING: Console code page (437) differs from Windows code page (1252)
         8-bit characters might not work correctly. See psql reference
         page "Notes for Windows users" for details.
Type "help" for help.
postgres=# ALTER USER postgres PASSWORD 'postgres';
ALTER ROLE
postgres=# create user adminuser with encrypted password 'admin';
CREATE ROLE
postgres=# grant all privileges on database postgres to adminuser;
GRANT
```
<br><br>
Perform migrations and migrate your tables to pg db like so:
<br>
```docker-compose run web python manage.py migrate```
<br><br>
Now Run the application using :
```docker-compose up```