from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from .serializers import UserDataSerializer, UserRegistrationSerializer
from .models import UserData
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
import coreapi
from rest_framework.schemas import AutoSchema
from rest_framework import generics
from rest_framework import mixins

class GenericAPIViews(generics.GenericAPIView, mixins.ListModelMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, mixins.RetrieveModelMixin):
    serializer_class = UserDataSerializer
    queryset = UserData.objects.all()
    lookup_field = 'UserId'
    def get(self, request, UserId=None):
        if UserId:
            return self.retrieve(request)
        else:
            return self.list(request)

    def put(self, request, UserId=None):
        return self.update(request, UserId)

    def delete(self, request, UserId):
        return self.destroy(request, UserId)


class UserRegistration(generics.GenericAPIView, mixins.CreateModelMixin):
    serializer_class = UserRegistrationSerializer
    def post(self, request):
         return self.create(request)

class GetAllUsers(generics.GenericAPIView, mixins.ListModelMixin):
    serializer_class = UserDataSerializer
    queryset = UserData.objects.all()
    def get(self, request):
        return self.list(request)


"""class UserDataAPIView(APIView):

    def get(self,request):
        userdata=UserData.objects.all()
        serializer=UserDataSerializer(userdata, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer=UserDataSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserDetails(APIView):
    def get_object(self, id):
        try:
            return UserData.objects.get(UserId=id)
        except:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        userdata=self.get_object(id)
        serializer = UserDataSerializer(userdata)
        return Response(serializer.data)

    def put(self, request, id):
        userdata=self.get_object(id)
        data = JSONParser().parse(request)
        serializer = UserDataSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        userdata=self.get_object(id)
        userdata.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

"""
"""
# Function based
@api_view(["POST"])
def UserRegistration(request):
    if request.method=="POST":
        data=JSONParser().parse(request)
        serializer=UserDataSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    else:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)


@api_view(["GET"])
def GetUserById(request,pk):
    try:
        userdata=UserData.objects.get(pk=pk)
    except UserData.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method=="GET":
        serializer = UserDataSerializer(userdata)
        return Response(serializer.data)
    else:
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

@api_view(["PUT"])
def UpdateUserById(request, pk):
    try:
        userdata=UserData.objects.get(pk=pk)
    except UserData.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method=="PUT":
        data=JSONParser().parse(request)
        serializer=UserDataSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    else:
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

@api_view(["DELETE"])
def DeleteUserById(request, pk):
    try:
        userdata = UserData.objects.get(pk=pk)
    except UserData.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method=="DELETE":
        userdata.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
"""