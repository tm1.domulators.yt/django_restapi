from rest_framework import serializers
from .models import UserData


class UserDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserData
        fields = ["UserId", "UserFirstName", "UserLastName", "UserEmail", "UserGender", "UserMobileNumber"]


class UserRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserData
        fields = ["UserFirstName", "UserLastName", "UserEmail", "UserGender", "UserMobileNumber"]
