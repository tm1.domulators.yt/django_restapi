from django.urls import path
from .views import GenericAPIViews, UserRegistration, GetAllUsers

urlpatterns = [
    #path('UserData/',UserDataAPIView.as_view()),
    #path('UserDetails/<int:id>/',UserDetails.as_view()),
    path('UserDetails/<int:UserId>/',GenericAPIViews.as_view()),
    path('UserRegistration/',UserRegistration.as_view()),
    path('GetAllUsers/',GetAllUsers.as_view()),
]
