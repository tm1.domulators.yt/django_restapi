from django.db import models


class UserData(models.Model):
    UserId = models.AutoField(primary_key=True)
    UserFirstName = models.CharField(max_length=50)
    UserLastName = models.CharField(max_length=50)
    UserEmail = models.EmailField(max_length=50)
    GENDERCHOICE = [("M", "Male"), ("F", "Female"), ("O", "Others")]
    UserGender = models.CharField(max_length=2, choices=GENDERCHOICE)
    UserMobileNumber = models.CharField(max_length=12)

    def __str__(self):
        return str(self.UserId)
